package com.gitlab.redeyes.theevent.dao.auth

import com.gitlab.redeyes.theevent.dao.BaseDaoTest
import com.gitlab.redeyes.theevent.dao.UserTest
import com.gitlab.redeyes.theevent.model.Role
import com.gitlab.redeyes.theevent.model.User
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test

internal class AuthDaoTest : BaseDaoTest(), UserTest {
    val emailSuffix: String = "@s.z"

    @Test
    internal fun testSave() {
        val prefix = "testSave"
        val user = testUser(prefix)
        assertNotNull(user.id)
    }

    @Test
    internal fun testSearchById() {
        val prefix = "testSearchById"
        val user = testUser(prefix)
        assertNotNull(user.id)
        val optional = userDao.findById(user.id!!)
        assertThat(optional).isNotEmpty.hasValueSatisfying { Assertions.assertEquals(prefix + emailSuffix, it.email) }
    }


    @Test
    internal fun testFindUserByName() {
        val user = testUser("testFindUserByName")
        val optional = authDao.findAuthUserByUserName(user.email)
        assertThat(optional).isNotNull
    }

    private fun testUser(prefix: String): User {
        val roles = listOf(roleDao.save(Role(prefix)))
        val email = "$prefix$emailSuffix"
        return newUser(userDao, email, roles, newOrg(orgDao, prefix))
    }
}
