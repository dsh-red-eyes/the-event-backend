package com.gitlab.redeyes.theevent.dao

import com.gitlab.redeyes.theevent.model.Role
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
internal class UserDaoTest : BaseDaoTest(), UserTest {

    @Test
    internal fun testSave() {
        val roles = listOf(roleDao.save(Role("testSaveUserDao")))
        val email = "testSaveUserDao@s.z"
        val user = newUser(userDao, email, roles, newOrg(orgDao, "testSaveUserDao"))
        assertNotNull(user.id)
    }

    @Test
    internal fun testSearchById() {
        val roles = listOf(roleDao.save(Role("testUserDaoSearchById")))
        val email = "testUserDaoSearchById@s.z"
        val user = newUser(userDao, email, roles, newOrg(orgDao, "testUserDaoSearchById"))
        assertNotNull(user.id)
        val optional = userDao.findById(user.id!!)
        assertThat(optional).isNotEmpty.hasValueSatisfying { assertEquals(email, it.email) }
    }
}
