package com.gitlab.redeyes.theevent.dao

import com.gitlab.redeyes.theevent.bot.TelegramBotNotifier
import com.gitlab.redeyes.theevent.dao.auth.AuthDao
import com.gitlab.redeyes.theevent.dao.auth.RoleDao
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.meta.generics.LongPollingBot

@SpringBootTest
abstract class BaseDaoTest {

    @Autowired
    lateinit var userDao: UserDao

    @Autowired
    lateinit var eventDao: EventDao

    @Autowired
    lateinit var eventBudget: EventBudgetDao

    @Autowired
    lateinit var authDao: AuthDao

    @Autowired
    lateinit var roleDao: RoleDao

    @Autowired
    lateinit var orgDao: OrganizationDao
}