package com.gitlab.redeyes.theevent.dao

import com.gitlab.redeyes.theevent.model.Organization
import com.gitlab.redeyes.theevent.model.Role
import com.gitlab.redeyes.theevent.model.User

interface UserTest {
    fun newUser(userDao: UserDao, email: String, roles: List<Role>, organization: Organization): User {
        return userDao.save(
                User("fname", "lname", email, organization, "1", roles)
        )
    }

    fun newOrg(orgDao: OrganizationDao, name: String): Organization {
        return orgDao.save(Organization(name))
    }
}
