from sklearn import feature_extraction
from sklearn import linear_model
from sklearn import pipeline
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.externals import joblib

import csv


def get_dataset(csv_file):
    X = []
    Y = []
    with open(csv_file) as csvfile:
        readCSV = csv.reader(csvfile, delimiter='\n')
        for row in readCSV:
            splitted = row[0].split(',')
            X.append(splitted[1])
            Y.append(splitted[2])

    # print("\n\nData set features {0}".format(len(X)))
    # print("Data set labels   {0}\n".format(len(Y)))

    return X, Y


X, Y = get_dataset("event.csv")


X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=0)
vectorizer = feature_extraction.text.TfidfVectorizer(ngram_range=(1, 4), analyzer='char')

pipe = pipeline.Pipeline([('vectorizer', vectorizer), ('clf', linear_model.LogisticRegression())])

pipe.fit(X_train, Y_train)

Y_predicted = pipe.predict(X_test)

# print(metrics.classification_report(Y_test, Y_predicted))

joblib.dump(pipe, 'event_model.pkl')
