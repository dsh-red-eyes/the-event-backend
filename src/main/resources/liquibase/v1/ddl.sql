CREATE SEQUENCE hibernate_sequence;
-- Drop table

-- DROP TABLE public.media;

CREATE TABLE public.media (
	id int8 NOT NULL,
	"name" varchar(255) NULL,
	pg int4 NOT NULL,
	CONSTRAINT media_pkey PRIMARY KEY (id)
);

-- Drop table

-- DROP TABLE public.media_requirements;

CREATE TABLE public.media_requirements (
	id int8 NOT NULL,
	pg int4 NOT NULL,
	CONSTRAINT media_requirements_pkey PRIMARY KEY (id)
);

-- Drop table

-- DROP TABLE public.organizations;

CREATE TABLE public.organizations (
	id int8 NOT NULL,
	"name" varchar(255) NULL,
	CONSTRAINT organizations_pkey PRIMARY KEY (id)
);

-- Drop table

-- DROP TABLE public.places;

CREATE TABLE public.places (
	id int8 NOT NULL,
	address varchar(255) NOT NULL,
	capacity int4 NOT NULL,
	"name" varchar(255) NOT NULL,
	CONSTRAINT places_pkey PRIMARY KEY (id)
);

-- Drop table

-- DROP TABLE public.resource_types;

CREATE TABLE public.resource_types (
	id int8 NOT NULL,
	description varchar(255) NULL,
	"name" varchar(255) NULL,
	CONSTRAINT resource_types_pkey PRIMARY KEY (id)
);

-- Drop table

-- DROP TABLE public.roles;

CREATE TABLE public.roles (
	id int8 NOT NULL,
	"name" varchar(255) NULL,
	CONSTRAINT auth_role_contr UNIQUE (name),
	CONSTRAINT roles_pkey PRIMARY KEY (id)
);

-- Drop table

-- DROP TABLE public.sponsor;

CREATE TABLE public.sponsor (
	id int8 NOT NULL,
	budget int8 NOT NULL,
	"name" varchar(255) NULL,
	pgs int4 NOT NULL,
	CONSTRAINT sponsor_pkey PRIMARY KEY (id)
);

-- Drop table

-- DROP TABLE public.sport_type;

CREATE TABLE public.sport_type (
	id int8 NOT NULL,
	"name" varchar(255) NULL,
	CONSTRAINT sport_type_pkey PRIMARY KEY (id)
);

-- Drop table

-- DROP TABLE public.places_sport_types;

CREATE TABLE public.places_sport_types (
	place_id int8 NOT NULL,
	sport_types_id int8 NOT NULL,
	CONSTRAINT uk_b950il8ybdly5k786otincnhu UNIQUE (sport_types_id),
	CONSTRAINT fkiop1lxkjlvpidj68bnnyjuebr FOREIGN KEY (sport_types_id) REFERENCES sport_type(id),
	CONSTRAINT fkrq6kqu68ilrsq1quss4nj2o6a FOREIGN KEY (place_id) REFERENCES places(id)
);

-- Drop table

-- DROP TABLE public.users;

CREATE TABLE public.users (
	id int8 NOT NULL,
	email varchar(255) NOT NULL,
	fname varchar(255) NULL,
	lname varchar(255) NULL,
	"password" varchar(255) NULL,
	organization_id int8 NULL,
	CONSTRAINT email_contr UNIQUE (email),
	CONSTRAINT users_pkey PRIMARY KEY (id),
	CONSTRAINT fkqpugllwvyv37klq7ft9m8aqxk FOREIGN KEY (organization_id) REFERENCES organizations(id)
);

-- Drop table

-- DROP TABLE public.users_roles;

CREATE TABLE public.users_roles (
	user_id int8 NOT NULL,
	roles_id int8 NOT NULL,
	CONSTRAINT fk2o0jvgh89lemvvo17cbqvdxaa FOREIGN KEY (user_id) REFERENCES users(id),
	CONSTRAINT fka62j07k5mhgifpp955h37ponj FOREIGN KEY (roles_id) REFERENCES roles(id)
);

-- Drop table

-- DROP TABLE public.volunteers;

CREATE TABLE public.volunteers (
	id int8 NOT NULL,
	contact varchar(255) NULL,
	"name" varchar(255) NULL,
	organization_id int8 NULL,
	CONSTRAINT volunteers_pkey PRIMARY KEY (id),
	CONSTRAINT fkpmh5oe86wwpe9lfx9j0mkqlk9 FOREIGN KEY (organization_id) REFERENCES organizations(id)
);

-- Drop table

-- DROP TABLE public.check_list_items;

CREATE TABLE public.check_list_items (
	id int8 NOT NULL,
	completed bool NOT NULL,
	"name" varchar(255) NOT NULL,
	spent float8 NOT NULL,
	assignee_id int8 NULL,
	author_id int8 NULL,
	parent_id int8 NULL,
	CONSTRAINT check_list_items_pkey PRIMARY KEY (id),
	CONSTRAINT fk5a94pg5f39sgj2iwa68ybi170 FOREIGN KEY (assignee_id) REFERENCES users(id),
	CONSTRAINT fkb3t7q7e1lp08gfjpmvxihwl35 FOREIGN KEY (parent_id) REFERENCES check_list_items(id),
	CONSTRAINT fkhst5dkfs2fm3g5bd4si92t39k FOREIGN KEY (author_id) REFERENCES users(id)
);

-- Drop table

-- DROP TABLE public.events;

CREATE TABLE public.events (
	id int8 NOT NULL,
	budget int8 NOT NULL,
	end_date timestamp NULL,
	"name" varchar(255) NOT NULL,
	planned_members_count int4 NOT NULL,
	start_date timestamp NULL,
	state int4 NULL,
	owner_id int8 NULL,
	responsible_id int8 NULL,
	sport_type_id int8 NULL,
	CONSTRAINT event_idx UNIQUE (name),
	CONSTRAINT events_pkey PRIMARY KEY (id),
	CONSTRAINT fk96020fdrh2ek9xdmoe9of7tdy FOREIGN KEY (owner_id) REFERENCES users(id),
	CONSTRAINT fka7vbmj6l58jyb7dhhuhbjxqrt FOREIGN KEY (sport_type_id) REFERENCES sport_type(id),
	CONSTRAINT fklaqkx15hr7wqcskyerol3h4ex FOREIGN KEY (responsible_id) REFERENCES users(id)
);

-- Drop table

-- DROP TABLE public.events_organizators;

CREATE TABLE public.events_organizators (
	event_id int8 NOT NULL,
	organizators_id int8 NOT NULL,
	CONSTRAINT uk_39x73gapvai3wid4eq0om0ox0 UNIQUE (organizators_id),
	CONSTRAINT fkmbo4vr7bud29eurrqu6aa8xqo FOREIGN KEY (event_id) REFERENCES events(id),
	CONSTRAINT fkq710hkdayb6t46wevtpsve098 FOREIGN KEY (organizators_id) REFERENCES users(id)
);

-- Drop table

-- DROP TABLE public.events_organizing_committee;

CREATE TABLE public.events_organizing_committee (
	event_id int8 NOT NULL,
	organizing_committee_id int8 NOT NULL,
	CONSTRAINT uk_ku7l3gydh09e1grcfpcwe7849 UNIQUE (organizing_committee_id),
	CONSTRAINT fk2q2w2rdk1gopmwy58cvcr2kxs FOREIGN KEY (organizing_committee_id) REFERENCES users(id),
	CONSTRAINT fkip640wju626vy1fxyv21eipi4 FOREIGN KEY (event_id) REFERENCES events(id)
);

-- Drop table

-- DROP TABLE public.events_volunteers;

CREATE TABLE public.events_volunteers (
	event_id int8 NOT NULL,
	volunteers_id int8 NOT NULL,
	CONSTRAINT uk_4jeyb35r0ot45317uo2hdqidq UNIQUE (volunteers_id),
	CONSTRAINT fk3tfyaoggs42ulx9nlduc4qu5k FOREIGN KEY (event_id) REFERENCES events(id),
	CONSTRAINT fktj1tx9yneu3w5vpasjre4acp8 FOREIGN KEY (volunteers_id) REFERENCES volunteers(id)
);

-- Drop table

-- DROP TABLE public.place_requirements;

CREATE TABLE public.place_requirements (
	id int8 NOT NULL,
	capacity int4 NULL,
	city varchar(255) NULL,
	event_id int8 NULL,
	CONSTRAINT place_requirements_pkey PRIMARY KEY (id),
	CONSTRAINT fkl3nhy9xyx9qda9mgc1e7wsfms FOREIGN KEY (event_id) REFERENCES events(id)
);

-- Drop table

-- DROP TABLE public.place_requirements_sport_types;

CREATE TABLE public.place_requirements_sport_types (
	place_requirements_id int8 NOT NULL,
	sport_types_id int8 NOT NULL,
	CONSTRAINT uk_ff9ocmq2fqi0y6ijdfkk4i3vd UNIQUE (sport_types_id),
	CONSTRAINT fk4ns3mb5dareb2bp018qtybemj FOREIGN KEY (sport_types_id) REFERENCES sport_type(id),
	CONSTRAINT fk9wj13afh2xtrwnbbuumr6xur3 FOREIGN KEY (place_requirements_id) REFERENCES place_requirements(id)
);

-- Drop table

-- DROP TABLE public.resources;

CREATE TABLE public.resources (
	id int8 NOT NULL,
	budget float8 NULL,
	requirements varchar(255) NULL,
	resolution varchar(255) NULL,
	state int4 NULL,
	assignee_id int8 NULL,
	event_id int8 NULL,
	type_id int8 NULL,
	CONSTRAINT resources_pkey PRIMARY KEY (id),
	CONSTRAINT fkdqkhgv2jqe49pclafokuwq1xi FOREIGN KEY (event_id) REFERENCES events(id),
	CONSTRAINT fkht2j97h4ch4t7uocat4sokuw1 FOREIGN KEY (assignee_id) REFERENCES users(id),
	CONSTRAINT fkogxcdp1pf7qdnxx6qlepfhrc8 FOREIGN KEY (type_id) REFERENCES resource_types(id)
);

-- Drop table

-- DROP TABLE public.sponsor_requirements;

CREATE TABLE public.sponsor_requirements (
	id int8 NOT NULL,
	budget int8 NOT NULL,
	pg int4 NOT NULL,
	event_id int8 NULL,
	CONSTRAINT sponsor_requirements_pkey PRIMARY KEY (id),
	CONSTRAINT fkp37lrmrb6oulydate9ki8lofs FOREIGN KEY (event_id) REFERENCES events(id)
);

-- Drop table

-- DROP TABLE public.tasks;

CREATE TABLE public.tasks (
	id int8 NOT NULL,
	description varchar(255) NULL,
	"name" varchar(255) NULL,
	state int4 NULL,
	assignee_id int8 NULL,
	creator_id int8 NULL,
	event_id int8 NULL,
	resource_id int8 NULL,
	CONSTRAINT tasks_pkey PRIMARY KEY (id),
	CONSTRAINT fkekr1dgiqktpyoip3qmp6lxsit FOREIGN KEY (assignee_id) REFERENCES users(id),
	CONSTRAINT fkoucetjmqkm50le1v0cstk6k38 FOREIGN KEY (resource_id) REFERENCES resources(id),
	CONSTRAINT fkso3gaw278lqccjow3yxkck6wx FOREIGN KEY (event_id) REFERENCES events(id),
	CONSTRAINT fkt1ph5sat39g9lpa4g5kl46tbv FOREIGN KEY (creator_id) REFERENCES users(id)
);

-- Drop table

-- DROP TABLE public.team;

CREATE TABLE public.team (
	id int8 NOT NULL,
	"name" varchar(255) NULL,
	captain_id int8 NULL,
	CONSTRAINT team_name_constr UNIQUE (name),
	CONSTRAINT team_pkey PRIMARY KEY (id),
	CONSTRAINT fkb1pjk4pg0yi1qvw0i7pvfy3us FOREIGN KEY (captain_id) REFERENCES users(id)
);

-- Drop table

-- DROP TABLE public.team_participants;

CREATE TABLE public.team_participants (
	team_id int8 NOT NULL,
	participants_id int8 NOT NULL,
	CONSTRAINT uk_997ef6693whayhxw4sndmseqm UNIQUE (participants_id),
	CONSTRAINT fkgf0hc4kjj03y8wtu8bydx9top FOREIGN KEY (participants_id) REFERENCES users(id),
	CONSTRAINT fksv1vo6sd6qxa9wbds3s9o4sw3 FOREIGN KEY (team_id) REFERENCES team(id)
);

-- Drop table

-- DROP TABLE public.check_list_items_resources;

CREATE TABLE public.check_list_items_resources (
	check_list_item_id int8 NOT NULL,
	resources_id int8 NOT NULL,
	CONSTRAINT uk_q5dcw9hfxib6klunkpw6atlkt UNIQUE (resources_id),
	CONSTRAINT fk2ehwashyo3bwcs5ibhm5f24bp FOREIGN KEY (check_list_item_id) REFERENCES check_list_items(id),
	CONSTRAINT fkgr0jmfts91x53hmayrat6jdg9 FOREIGN KEY (resources_id) REFERENCES resources(id)
);

-- Drop table

-- DROP TABLE public.check_list_plan;

CREATE TABLE public.check_list_plan (
	id int8 NOT NULL,
	"name" varchar(255) NOT NULL,
	author_id int8 NULL,
	event_id int8 NULL,
	root_item_id int8 NULL,
	CONSTRAINT check_list_plan_pkey PRIMARY KEY (id),
	CONSTRAINT fk85qip0j96hof176fcncuw83y3 FOREIGN KEY (author_id) REFERENCES users(id),
	CONSTRAINT fkjpo5mt8sjd8q7sfk8lfc7c01f FOREIGN KEY (event_id) REFERENCES events(id),
	CONSTRAINT fkr0qssjusqv6iiccxgc3vw7wcb FOREIGN KEY (root_item_id) REFERENCES check_list_items(id)
);

-- Drop table

-- DROP TABLE public.event_budget;

CREATE TABLE public.event_budget (
	id int8 NOT NULL,
	percentage int4 NOT NULL,
	"type" int4 NULL,
	volume int8 NOT NULL,
	event_id int8 NULL,
	CONSTRAINT event_budget_pkey PRIMARY KEY (id),
	CONSTRAINT fko5n36tklweuog5ly83aaxlnx9 FOREIGN KEY (event_id) REFERENCES events(id)
);


-- Drop table

-- DROP TABLE public.telegram_users;

CREATE TABLE public.telegram_users (
	id int8 NOT NULL,
	email varchar(255) NULL,
	CONSTRAINT telegram_users_pkey PRIMARY KEY (id)
);
