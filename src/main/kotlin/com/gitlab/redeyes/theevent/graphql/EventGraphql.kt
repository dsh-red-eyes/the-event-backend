package com.gitlab.redeyes.theevent.graphql

import com.expediagroup.graphql.spring.operations.Mutation
import com.expediagroup.graphql.spring.operations.Query
import com.gitlab.redeyes.theevent.dao.EventDao
import com.gitlab.redeyes.theevent.dao.SportTypeDao
import com.gitlab.redeyes.theevent.dao.UserDao
import com.gitlab.redeyes.theevent.model.BotNotifier
import com.gitlab.redeyes.theevent.model.Event
import com.gitlab.redeyes.theevent.model.EventState
import com.gitlab.redeyes.theevent.model.SportType
import org.springframework.data.domain.PageRequest
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class EventQuery(val eventDao: EventDao) : Query {

    fun eventsTotal(): Long {
        return eventDao.count()
    }

    fun eventFindAll(page: Int, count: Int): List<Event> {
        return eventDao.findAll(PageRequest.of(page, count)).content
    }

    fun eventFindById(id: Long): Event? {
        return eventDao.searchById(id)
    }

    fun eventFindByName(name: String): List<Event> {
        return eventDao.findByName(name)
    }
}

data class PlainEvent(val id: Long?,
                      val name: String,
                      val sportType: SportType,
                      val startDate: LocalDateTime,
                      val endDate: LocalDateTime,
                      var plannedMembersCount: Int,
                      var budget: Long,
                      val organizingCommittee: List<UserReference>?
)

@Component
class EventMutations(val eventDao: EventDao, val userDao: UserDao, val sportTypeDao: SportTypeDao, val bots: List<BotNotifier>) : Mutation {
    @PreAuthorize("hasAnyRole('EVENT_OWNER')")
    fun eventSave(event: PlainEvent, context: MyGraphQLContext): Event {
        val user = userDao.findByName(context.authentication!!.principal as String)!!
        var sportType = sportTypeDao.findByName(event.sportType.name)
        if (sportType == null) {
            sportType = sportTypeDao.save(SportType(event.sportType.name))
        }
        val newEvent = if (event.id == null || event.id == 0L) {
            Event(name = event.name,
                    owner = user,
                    startDate = event.startDate,
                    endDate = event.endDate,
                    plannedMembersCount = event.plannedMembersCount,
                    budget = event.budget,
                    organizingCommittee = if (event.organizingCommittee != null) {
                        userDao.findAllByIds(event.organizingCommittee.mapNotNull { it.id })
                    } else {
                        emptyList()
                    },
                    responsible = user,
                    sportType = sportType,
                    state = EventState.CREATED,
                    organizators = emptyList(),
                    volunteers = emptyList()
            )
        } else {
            val found = eventDao.findById(event.id).orElseThrow { IllegalArgumentException("Not found by id " + event.id) }
            found.startDate = event.startDate
            found.endDate = event.endDate
            found.plannedMembersCount = event.plannedMembersCount
            found.budget = event.budget
            found.organizingCommittee = if (event.organizingCommittee != null) {
                userDao.findAllByIds(event.organizingCommittee.mapNotNull { it.id })
            } else {
                emptyList()
            }
            found.sportType = sportType
            found
        }
        return eventDao.save(newEvent)
    }

    @PreAuthorize("hasAnyRole('EVENT_OWNER', 'EVENT_EDITOR')")
    fun eventUpdate(event: Event): Event {
        return eventDao.save(event)
    }

    @PreAuthorize("hasAnyRole('EVENT_OWNER', 'EVENT_EDITOR')")
    fun eventSetCommittee(eventId: Long, committee: List<UserReference>): List<UserReference> {
        val event = eventDao.findById(eventId).orElseThrow()
        event.organizingCommittee = userDao.findAllByIds(committee.mapNotNull { it.id })
        eventDao.save(event)
        return if (event.organizingCommittee != null) {
            event.organizingCommittee!!.forEach {
                bots.forEach { bot -> bot.notify(it.email, "Congratulations, you are assigned on event ${event.name}") }
            }
            event.organizingCommittee!!.map { UserReference.of(it) }
        } else {
            emptyList()
        }
    }
}
