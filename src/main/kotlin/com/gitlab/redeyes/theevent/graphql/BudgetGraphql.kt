package com.gitlab.redeyes.theevent.graphql

import com.expediagroup.graphql.spring.operations.Mutation
import com.expediagroup.graphql.spring.operations.Query
import com.gitlab.redeyes.theevent.dao.EventBudgetDao
import com.gitlab.redeyes.theevent.dao.EventDao
import com.gitlab.redeyes.theevent.model.EventBudget
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component

@Component
class BudgetQuery(val dao: EventBudgetDao) : Query {

}

@Component
class BudgetMutations(val dao: EventBudgetDao, val eventDao: EventDao) : Mutation {
    @PreAuthorize("hasAnyRole('EVENT_EDITOR')")
    fun eventBudgetCreate(budget: EventBudget, eventId: Long): EventBudget? {
        val event = eventDao.searchById(eventId)
        budget.event = event!!
        return dao.save(budget)
    }

    @PreAuthorize("hasAnyRole('EVENT_OWNER', 'EVENT_EDITOR')")
    fun eventBudgetUpdate(event: EventBudget): EventBudget {
        return dao.save(event)
    }
}
