package com.gitlab.redeyes.theevent.graphql

import com.expediagroup.graphql.spring.operations.Mutation
import com.expediagroup.graphql.spring.operations.Query
import com.gitlab.redeyes.theevent.dao.MediaDao
import com.gitlab.redeyes.theevent.model.Media
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component

@Component
class MediaQuery(val dao: MediaDao) : Query {
    fun mediaFindByName(name: String): List<Media> {
        return dao.findByName(name)
    }

    fun mediaFindById(id: Long): Media? {
        return dao.searchById(id)
    }
}

@Component
class MediaMutations(val dao: MediaDao) : Mutation {
    @PreAuthorize("hasAnyRole('ADMIN')")
    fun mediaCreate(media: Media): Media? {
        return dao.save(media)
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    fun mediaUpdate(event: Media): Media {
        return dao.save(event)
    }
}
