package com.gitlab.redeyes.theevent.graphql

import com.expediagroup.graphql.spring.operations.Mutation
import com.expediagroup.graphql.spring.operations.Query
import com.gitlab.redeyes.theevent.dao.OrganizationDao
import com.gitlab.redeyes.theevent.dao.UserDao
import com.gitlab.redeyes.theevent.dao.auth.RoleDao
import com.gitlab.redeyes.theevent.model.Identified
import com.gitlab.redeyes.theevent.model.Role
import com.gitlab.redeyes.theevent.model.User
import org.springframework.data.domain.PageRequest
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component

@Component
class UserQuery(val userDao: UserDao) : Query {

    fun usersTotal(): Long {
        return userDao.count()
    }

    fun userFindAll(page: Int, count: Int): List<User> {
        return userDao.findAll(PageRequest.of(page, count)).content
    }

    fun userFindById(id: Long): PlainUser? {
        return userDao.findById(id).map { PlainUser.of(it) }.orElse(null)
    }

    fun userFindByName(name: String): User? {
        return userDao.findByName(name)
    }
}

@Component
class Me(val userDao: UserDao) : Query {
    @PreAuthorize("hasAnyRole('USER')")
    fun me(context: MyGraphQLContext): User? {
        return userDao.findByName(context.authentication!!.principal as String)
    }
}

data class PlainUser(val fname: String,
                     val lname: String,
                     val email: String,
                     val organization: Long?,
                     val password: String,
                     val roles: List<Long>
): Identified(null) {
    companion object {
        fun of(user: User): PlainUser = PlainUser(fname = user.fname,
                    lname = user.lname,
                    email = user.email,
                    organization = user.organization?.id,
                    password = "**********************",
                    roles = user.roles.mapNotNull { it.id }
        ).also { it.id = user.id }
    }
}

data class UserReference(val id: Long?,
                         val fname: String?,
                         val lname: String?,
                         val email: String?,
                         val organization: Long?,
                         val roles: List<Role>?
){
    companion object {
        fun of(user: User): UserReference = UserReference(
                id = user.id,
                fname = user.fname,
                lname = user.lname,
                email = user.email,
                organization = user.organization?.id,
                roles = user.roles
        )
    }
}

@Component
class UserMutations(val userDao: UserDao,
                    val passwordEncoder: PasswordEncoder,
                    val organizationDao: OrganizationDao,
                    val roleDao: RoleDao
) : Mutation {
    @PreAuthorize("hasAnyRole('ADMIN')")
    fun userSave(user: PlainUser): PlainUser {
        val toSave = if (user.id == null || user.id == 0L) {
            User(
                    fname = user.fname,
                    lname = user.lname,
                    password = passwordEncoder.encode(user.password),
                    email = user.email,
                    organization = if (user.organization != null) {
                        organizationDao.searchById(user.organization)
                    } else {
                        null
                    },
                    roles = roleDao.findAllByIds(user.roles)
                    )
        } else {
            val userToUpdate = userDao.searchById(user.id!!)!!
            userToUpdate.fname = user.fname
            userToUpdate.lname = user.lname
            userToUpdate.roles = roleDao.findAllByIds(user.roles)
            if (user.password != "**********************") {
                userToUpdate.password = passwordEncoder.encode(user.password)
            }
            userToUpdate
        }
        return PlainUser.of(userDao.save(toSave))
    }
}

@Component
class RolesQuery(
        val roleDao: RoleDao
): Query {
    fun rolesAll(): List<Role> {
        return roleDao.findAll(PageRequest.of(0, 100)).content
    }
}
