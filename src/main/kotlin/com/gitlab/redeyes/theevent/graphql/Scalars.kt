package com.gitlab.redeyes.theevent.graphql

import com.expediagroup.graphql.hooks.SchemaGeneratorHooks
import graphql.language.StringValue
import graphql.schema.Coercing
import graphql.schema.GraphQLScalarType
import graphql.schema.GraphQLType
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.reflect.KClass
import kotlin.reflect.KType


@Component
class GeneratorHooks : SchemaGeneratorHooks {
    override fun willGenerateGraphQLType(type: KType): GraphQLType? = when (type.classifier as? KClass<*>) {
        LocalDateTime::class -> localDateTime
        else -> null
    }
}

val localDateTime = GraphQLScalarType.newScalar()
        .name("LocalDateTime")
        .description("A type representing a formatted java.time.LocalDateTime. Format yyyy-MM-dd HH:mm:ss (ISO)")
        .coercing(LocalDateTimeCoercing)
        .build()!!
val localDateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")!!

object LocalDateTimeCoercing : Coercing<LocalDateTime, String> {
    override fun parseValue(input: Any?): LocalDateTime = LocalDateTime.parse(serialize(input), localDateTimeFormatter)

    override fun parseLiteral(input: Any?): LocalDateTime? {
        val dateTimeString = (input as? StringValue)?.value
        return LocalDateTime.parse(dateTimeString, localDateTimeFormatter)
    }

    override fun serialize(dataFetcherResult: Any?): String? = when(dataFetcherResult) {
        is LocalDateTime -> dataFetcherResult.format(localDateTimeFormatter)
        is String ->  dataFetcherResult
        else -> throw IllegalArgumentException("Wrong value")
    }
}
