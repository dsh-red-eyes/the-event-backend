package com.gitlab.redeyes.theevent.graphql

import com.expediagroup.graphql.spring.operations.Mutation
import com.expediagroup.graphql.spring.operations.Query
import com.gitlab.redeyes.theevent.dao.OrganizationDao
import com.gitlab.redeyes.theevent.dao.UserDao
import com.gitlab.redeyes.theevent.model.Organization
import com.gitlab.redeyes.theevent.model.User
import org.springframework.data.domain.PageRequest
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component

@Component
class OrganizationQuery(val organizationDao: OrganizationDao): Query {
    @PreAuthorize("hasAnyRole('USER')")
    fun organizationsTotal(): Long {
        return organizationDao.count()
    }

    @PreAuthorize("hasAnyRole('USER')")
    fun organizationFindAll(page: Int, count: Int): List<Organization> {
        return organizationDao.findAll(PageRequest.of(page, count)).content
    }

    @PreAuthorize("hasAnyRole('USER')")
    fun organizationFindById(id: Long): Organization? {
        return organizationDao.searchById(id)
    }
}

@Component
class OrganizationMutations(val organizationDao: OrganizationDao) : Mutation {
    @PreAuthorize("hasAnyRole('ADMIN')")
    fun organizationCreate(organization: Organization): Organization {
        return organizationDao.save(organization)
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    fun organizationUpdate(organization: Organization): Organization {
        return organizationDao.save(organization)
    }
}