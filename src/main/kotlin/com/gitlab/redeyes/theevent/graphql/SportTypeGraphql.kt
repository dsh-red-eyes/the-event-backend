package com.gitlab.redeyes.theevent.graphql

import com.expediagroup.graphql.spring.operations.Mutation
import com.expediagroup.graphql.spring.operations.Query
import com.gitlab.redeyes.theevent.dao.SportTypeDao
import com.gitlab.redeyes.theevent.model.SportType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component

@Component
class SportTypeQuery(val dao: SportTypeDao) : Query {
    fun sportTypeFindAll(): List<SportType> {
        return dao.findAll()
    }

    fun sportTypeFindById(id: Long): SportType? {
        return dao.searchById(id)
    }
}

@Component
class SportTypeMutations(val dao: SportTypeDao) : Mutation {
    @PreAuthorize("hasAnyRole('ADMIN')")
    fun sportTypeCreate(sportType: SportType): SportType? {
        return dao.save(sportType)
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    fun sportTypeUpdate(event: SportType): SportType {
        return dao.save(event)
    }
}
