package com.gitlab.redeyes.theevent.graphql

import com.expediagroup.graphql.execution.EmptyGraphQLContext
import com.expediagroup.graphql.execution.GraphQLContext
import com.expediagroup.graphql.spring.execution.GraphQLContextFactory
import com.gitlab.redeyes.theevent.model.auth.JwtUtils
import com.gitlab.redeyes.theevent.service.AuthUserDetailsService
import io.jsonwebtoken.Claims
import org.springframework.http.HttpHeaders
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.http.server.reactive.ServerHttpResponse
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.ReactiveSecurityContextHolder
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextImpl
import org.springframework.stereotype.Component

/**
 * Simple [GraphQLContext] that holds extra value.
 */
class MyGraphQLContext(val authentication: Authentication?, val request: ServerHttpRequest, val response: ServerHttpResponse, var subscriptionValue: String? = null) : GraphQLContext


@Component
class MyGraphQLContextFactory(val jwtUtil: JwtUtils, val authUserDetailsService: AuthUserDetailsService) : GraphQLContextFactory<MyGraphQLContext> {

    override suspend fun generateContext(request: ServerHttpRequest, response: ServerHttpResponse): MyGraphQLContext {
        val authHeader: String? = request.headers.getFirst(HttpHeaders.AUTHORIZATION)
        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            val authToken = authHeader.substring(7)
            val claims: Claims = jwtUtil.getAllClaimsFromToken(authToken)
            val userDetailMono = authUserDetailsService.findByUsername(claims.subject)
            return userDetailMono.map {
                MyGraphQLContext(
                        authentication = UsernamePasswordAuthenticationToken(claims.subject,authToken, it.authorities),
                        request = request,
                        response = response
                )
            }.block()!!
        }
        return MyGraphQLContext(authentication = null, request = request, response = response)
    }
}