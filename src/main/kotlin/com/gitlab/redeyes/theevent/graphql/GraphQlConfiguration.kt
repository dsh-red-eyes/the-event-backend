package com.gitlab.redeyes.theevent.graphql

import com.expediagroup.graphql.spring.DEFAULT_INSTRUMENTATION_ORDER
import com.expediagroup.graphql.spring.GraphQLConfigurationProperties
import graphql.ExecutionResult
import graphql.GraphQL
import graphql.execution.AsyncExecutionStrategy
import graphql.execution.DataFetcherExceptionHandler
import graphql.execution.ExecutionContext
import graphql.execution.ExecutionIdProvider
import graphql.execution.ExecutionStrategyParameters
import graphql.execution.NonNullableFieldWasNullException
import graphql.execution.instrumentation.ChainedInstrumentation
import graphql.execution.instrumentation.Instrumentation
import graphql.execution.preparsed.PreparsedDocumentProvider
import graphql.schema.GraphQLSchema
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.util.*
import java.util.concurrent.*


@Component("queryTransactionalStrategy")
class AsyncTransactionalExecutionStrategy(handler: DataFetcherExceptionHandler) : AsyncExecutionStrategy(handler) {
    @Transactional
    @Throws(NonNullableFieldWasNullException::class)
    override fun execute(executionContext: ExecutionContext, parameters: ExecutionStrategyParameters): CompletableFuture<ExecutionResult> {
        return super.execute(executionContext, parameters)
    }
}

@Configuration
class GraphQLConfig {
    @Bean
    fun graphQL(
            schema: GraphQLSchema,
            dataFetcherExceptionHandler: DataFetcherExceptionHandler,
            instrumentations: Optional<List<Instrumentation>>,
            executionIdProvider: Optional<ExecutionIdProvider>,
            preparsedDocumentProvider: Optional<PreparsedDocumentProvider>,
            config: GraphQLConfigurationProperties,
            @Qualifier("queryTransactionalStrategy") queryStrategy: AsyncExecutionStrategy
    ): GraphQL {
        val graphQL = GraphQL.newGraphQL(schema)
                .queryExecutionStrategy(queryStrategy)
                .mutationExecutionStrategy(AsyncExecutionStrategy(dataFetcherExceptionHandler))
                .subscriptionExecutionStrategy(AsyncExecutionStrategy(dataFetcherExceptionHandler))

        instrumentations.ifPresent { unordered ->
            if (unordered.size == 1) {
                graphQL.instrumentation(unordered.first())
            } else {
                val sorted = unordered.sortedBy {
                    if (it is Ordered) {
                        it.order
                    } else {
                        DEFAULT_INSTRUMENTATION_ORDER
                    }
                }
                graphQL.instrumentation(ChainedInstrumentation(sorted))
            }
        }
        executionIdProvider.ifPresent {
            graphQL.executionIdProvider(it)
        }
        preparsedDocumentProvider.ifPresent {
            graphQL.preparsedDocumentProvider(it)
        }
        return graphQL.build()
    }
}

data class UCPage<T>(val totalElements: Long, val content: List<T>) {
    companion object Factory {
        fun <T> of(page: Page<T>): UCPage<T> = UCPage(page.totalElements, page.content)
    }
}
