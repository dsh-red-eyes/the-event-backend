package com.gitlab.redeyes.theevent.graphql

import com.expediagroup.graphql.spring.operations.Mutation
import com.expediagroup.graphql.spring.operations.Query
import com.gitlab.redeyes.theevent.dao.CheckListItemDao
import com.gitlab.redeyes.theevent.dao.CheckListPlanDao
import com.gitlab.redeyes.theevent.dao.EventDao
import com.gitlab.redeyes.theevent.dao.UserDao
import com.gitlab.redeyes.theevent.model.BotNotifier
import com.gitlab.redeyes.theevent.model.CheckListItem
import com.gitlab.redeyes.theevent.model.CheckListPlan
import com.gitlab.redeyes.theevent.model.Resource
import com.gitlab.redeyes.theevent.model.User
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional


@Component
class PlanQuery(val planDao: CheckListPlanDao, val checkListItemDao: CheckListItemDao) : Query {
    fun planTotal(eventId: Long): Long {
        return planDao.countByEventId(eventId)
    }

    fun planFindAllByEventId(eventId: Long, page: Int, count: Int): List<CheckListPlan> {
        return planDao.findAllByEventId(eventId, PageRequest.of(page, count)).content
    }

    fun planFindById(id: Long): PlainPlan? {
        return planDao.searchById(id)?.let { PlainPlan.of(it) }
    }

    fun checkListFindSubItemsById(id: Long): List<PlainCheckListItem> {
        return checkListItemDao.findAllByParentId(id).map { PlainCheckListItem.of(it) }
    }
}

data class PlainCheckListItem(
        val id: Long?,
        val name: String?,
        val subItems: MutableList<PlainCheckListItem>?,
        val author: UserReference?,
        val assignee: UserReference?,
        val spent: Double?,
        val resources: List<Resource>?,
        val completed: Boolean?
) {
    companion object {
        fun of(item: CheckListItem): PlainCheckListItem {
            return PlainCheckListItem(
                    id = item.id,
                    name = item.name,
                    subItems = item.subItems.map { of(it) }.toMutableList(),
                    assignee = item.assignee?.let { UserReference.of(it) },
                    author = UserReference.of(item.author),
                    spent = item.spent,
                    resources = item.resources,
                    completed = item.completed
            )
        }
    }
}

data class PlainPlan(
        val id: Long,
        val eventId: Long,
        val name: String,
        val author: UserReference?,
        val root: PlainCheckListItem?
) {
    companion object {
        fun of(plan: CheckListPlan): PlainPlan {
            return PlainPlan(
                    id = plan.id!!,
                    eventId = plan.event.id!!,
                    author = UserReference.of(plan.author),
                    name = plan.name,
                    root = plan.root.let { PlainCheckListItem.of(it) }
            )
        }
    }
}

@Component
class PlanMutation(val planDao: CheckListPlanDao,
                   val eventDao: EventDao,
                   val userDao: UserDao,
                   val checkListItemDao: CheckListItemDao,
                   val notifiers: List<BotNotifier>
) : Mutation {

    @Transactional
    fun checkListUpdate(item: PlainCheckListItem): PlainCheckListItem {
        //only done and spent
        val toSave = checkListItemDao.findById(item.id!!).orElseThrow()
        item.completed?.let { toSave.completed = it }
        item.spent?.let { toSave.spent = it }
        return PlainCheckListItem.of(checkListItemDao.save(toSave).also {
            it.assignee?.let { notifiers.forEach { bot -> bot.notify(it.email, "Taks has been updated: completed=${toSave.completed}, spent=${toSave.spent}") }
        } })
    }

    @Transactional
    fun planSave(eventId: Long, plan: PlainPlan, context: MyGraphQLContext): PlainPlan {
        val user = userDao.findByName(context.authentication!!.principal as String)!!
        val toSave = if (plan.id == 0L) {
            val newPlan = CheckListPlan(
                    name = plan.name,
                    event = eventDao.findById(eventId).orElseThrow(),
                    author = user,
                    root = checkListItemDao.save(CheckListItem(
                            parent = null,
                            name = plan.name,
                            author = user,
                            completed = false,
                            resources = null,
                            spent = 0.0,
                            assignee = null,
                            subItems = mutableListOf()
                    ))
            )
            plan.root?.subItems?.let {
                newPlan.root.subItems.clear()
                newPlan.root.subItems.addAll(it.map { checkListItemDao.save(mapDtoToEntity(it, newPlan.root, user)) })
            }
            newPlan
        } else {
            val oldPlan = planDao.findById(plan.id).orElseThrow()
            oldPlan.name = plan.name
            plan.root?.subItems?.let {
                oldPlan.root.subItems.clear()
                oldPlan.root.subItems.addAll(it.map { checkListItemDao.save(mapDtoToEntity(it, oldPlan.root, user)) })
            }
            oldPlan
        }
        return PlainPlan.of(planDao.save(toSave))
    }

    private fun mapDtoToEntity(dto: PlainCheckListItem, parent: CheckListItem, user: User): CheckListItem {
        val result = CheckListItem(
                parent = parent,
                name = dto.name!!,
                author = if (dto.author != null) {
                    userDao.findById(dto.author.id!!).orElseThrow()
                } else {
                    user
                },
                completed = dto.completed != null && dto.completed,
                resources = dto.resources,
                spent = dto.spent ?: 0.0,
                assignee = dto.assignee?.id?.let { userDao.findById(it).orElseThrow() },
                subItems = mutableListOf()
        )
        result.id = dto.id
        result.assignee?.let {
            notifiers.forEach { bot -> bot.notify(it.email, "You are assigned on task ${result.name}") }
        }
        dto.subItems?.let {
            result.subItems.clear()
            result.subItems.addAll(it.map { mapDtoToEntity(it, result, user) })
        }
        return checkListItemDao.save(result)
    }
}
