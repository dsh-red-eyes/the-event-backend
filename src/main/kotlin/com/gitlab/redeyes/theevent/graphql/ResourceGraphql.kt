package com.gitlab.redeyes.theevent.graphql

import com.expediagroup.graphql.spring.operations.Mutation
import com.expediagroup.graphql.spring.operations.Query
import com.gitlab.redeyes.theevent.dao.*
import com.gitlab.redeyes.theevent.model.ApprovalProcessState
import com.gitlab.redeyes.theevent.model.Resource
import com.gitlab.redeyes.theevent.model.ResourceType
import com.gitlab.redeyes.theevent.model.Task
import org.springframework.data.domain.PageRequest
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component

@Component
class ResourceQuery(val resourceDao: ResourceDao,
                    var resourceTypeDao: ResourceTypeDao,
                    val eventDao: EventDao,
                    val userDao: UserDao) : Query {
    fun resourcesTotal(eventId: Long, context: MyGraphQLContext): Long {
        return resourceDao.countByEventId(eventId)
    }

    fun resourceFindAll(eventId: Long, page: Int, count: Int, context: MyGraphQLContext): List<Resource> {
        return resourceDao.findAllByEventId(eventId, PageRequest.of(page, count)).content
    }

    fun resourceTypeFindAll(): List<ResourceType> {
        return resourceTypeDao.findAll(PageRequest.of(0, 100)).content
    }
}

data class ResourcePlain(
        val id: Long?,
        var name: String?,
        var eventId: Long,
        var requirements: String?,
        var resolution: String?,
        var state: ApprovalProcessState?,
        var budget: Double?,
        var assignee: UserReference?,
        var type: ResourceType?
) {
    companion object {
        fun of(resource: Resource): ResourcePlain = ResourcePlain(
                id = resource.id,
                name = resource.name,
                eventId = resource.event.id!!,
                assignee = resource.assignee?.let { UserReference.of(it) },
                requirements = resource.requirements,
                resolution = resource.resolution,
                state = resource.state,
                budget = resource.budget,
                type = resource.type
        )
    }
}

@Component
class ResourceMutation(val resourceDao: ResourceDao,
                       var resourceTypeDao: ResourceTypeDao,
                       val eventDao: EventDao,
                       val userDao: UserDao) : Mutation {

    @PreAuthorize("hasAnyRole('EVENT_OWNER', 'EVENT_EDITOR')")
    fun resourceSave(eventId: Long, resource: ResourcePlain, context: MyGraphQLContext): Resource? {
        val toSave = if (resource.id == 0L) {
            val event = eventDao.searchById(eventId)!!
            val resourceType = if (resource.type?.id != null && resource.type!!.id!! != 0L) {
                resourceTypeDao.findById(resource.type!!.id!!).orElseThrow()
            } else {
                resourceTypeDao.save(ResourceType(name = resource.type!!.name, description = resource.type!!.name))
            }

            Resource(
                    event = event,
                    name = resource.name!!,
                    type = resourceType,
                    budget = resource.budget ?: 0.0,
                    resolution = resource.resolution,
                    requirements = resource.requirements,
                    assignee = userDao.findById(resource.assignee!!.id!!).orElse(null),
                    state = ApprovalProcessState.OPEN
            )
        } else {
            val toSave = resourceDao.findById(resource.id!!).orElseThrow()
            resource.name?.let { toSave.name = it }
            resource.requirements?.let { toSave.requirements = it }
            resource.resolution?.let { toSave.resolution = it }
            resource.budget?.let { toSave.budget = it }
            resource.assignee?.id?.let { toSave.assignee = userDao.findById(it).orElseThrow() }
            resource.state?.let { toSave.state = it }
            toSave
        }
        return resourceDao.save(toSave)
    }

}