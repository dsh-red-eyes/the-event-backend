package com.gitlab.redeyes.theevent.graphql

import com.expediagroup.graphql.spring.operations.Mutation
import com.expediagroup.graphql.spring.operations.Query
import com.gitlab.redeyes.theevent.dao.auth.RoleDao
import com.gitlab.redeyes.theevent.model.Role
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component

@Component
class RoleQuery(val roleDao: RoleDao) : Query {

    fun roleFindById(id: Long): Role? {
        return roleDao.searchById(id)
    }

    fun roleFindByName(name: String): Role? {
        return roleDao.findByName(name)
    }
}

@Component
class RoleMutations(val roleDao: RoleDao) : Mutation {
    @PreAuthorize("isAuthenticated()")
    fun roleCreate(role: Role): Role {
        return roleDao.save(role)
    }
}
