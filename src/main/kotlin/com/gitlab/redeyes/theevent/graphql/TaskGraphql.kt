package com.gitlab.redeyes.theevent.graphql

import com.expediagroup.graphql.spring.operations.Mutation
import com.expediagroup.graphql.spring.operations.Query
import com.gitlab.redeyes.theevent.dao.EventDao
import com.gitlab.redeyes.theevent.dao.TaskDao
import com.gitlab.redeyes.theevent.dao.UserDao
import com.gitlab.redeyes.theevent.model.ApprovalProcessState
import com.gitlab.redeyes.theevent.model.Resource
import com.gitlab.redeyes.theevent.model.Task
import org.springframework.data.domain.PageRequest
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component

@Component
class TaskQuery(val taskDao: TaskDao, val eventDao: EventDao, val userDao: UserDao) : Query {
    fun tasksTotal(eventId: Long, context: MyGraphQLContext): Long {
        val user = userDao.findByName(context.authentication!!.principal as String)!!
        val event = eventDao.searchById(eventId)!!
        return taskDao.countByAssigneeAndEvent(user, event)
    }

    fun taskFindAll(eventId: Long, page: Int, count: Int, context: MyGraphQLContext): List<Task> {
        val user = userDao.findByName(context.authentication!!.principal as String)!!
        val event = eventDao.searchById(eventId)!!
        return taskDao.findAllByAssigneeAndEvent(PageRequest.of(page, count), user, event).content
    }
}

data class TaskPlain(
        val id: Long?,
        var name: String?,
        var description: String?,
        var assignee: UserReference?,
        var resource: Resource?,
        var creator: UserReference?,
        var state: ApprovalProcessState?
) {
    companion object {
        fun of(task: Task): TaskPlain = TaskPlain(
                id = task.id,
                assignee = task.assignee?.let { UserReference.of(it) },
                name = task.name,
                description = task.description,
                resource = task.resource,
                creator = task.creator?.let { UserReference.of(it) },
                state = task.state
        )
    }
}

@Component
class TaskMutation(val taskDao: TaskDao, val eventDao: EventDao, val userDao: UserDao) : Mutation {

    @PreAuthorize("hasAnyRole('EVENT_OWNER', 'EVENT_EDITOR')")
    fun taskSave(eventId: Long, task: TaskPlain, context: MyGraphQLContext): TaskPlain? {
        val toSave = if (task.id == 0L) {
            val user = userDao.findByName(context.authentication!!.principal as String)!!
            val event = eventDao.searchById(eventId)!!
            Task(
                    event = event,
                    creator = user,
                    name = task.name!!,
                    description = task.description!!,
                    assignee = userDao.findById(task.assignee!!.id!!).orElseThrow(),
                    resource = task.resource,
                    state = ApprovalProcessState.OPEN
            )
        } else {
            val toSave = taskDao.findById(task.id!!).orElseThrow()
            task.name?.let { toSave.name = it }
            task.description?.let { toSave.description = it }
            task.resource?.let { toSave.resource = it }
            task.assignee?.id?.let { toSave.assignee = userDao.findById(it).orElseThrow() }
            task.state?.let { toSave.state = it }
            toSave
        }
        return TaskPlain.of(taskDao.save(toSave))
    }

}