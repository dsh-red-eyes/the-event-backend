package com.gitlab.redeyes.theevent.graphql

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.expediagroup.graphql.spring.operations.Mutation
import com.expediagroup.graphql.spring.operations.Query
import com.gitlab.redeyes.theevent.dao.SponsorDao
import com.gitlab.redeyes.theevent.dao.SponsorRequirementsDao
import com.gitlab.redeyes.theevent.model.Sponsor
import com.gitlab.redeyes.theevent.model.SponsorRequirements
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component


@Component
class SponsorQuery(val dao: SponsorDao) : Query {
    fun sponsorFindByName(name: String): List<Sponsor> {
        return dao.findByName(name)
    }

    fun sponsorFindBudgetGt(volume: Long): List<Sponsor> {
        return dao.findByBudgetGt(volume)
    }

    fun sponsorFindBudgetLt(volume: Long): List<Sponsor> {
        return dao.findByBudgetLt(volume)
    }

    fun sponsorFindById(id: Long): Sponsor? {
        return dao.searchById(id)
    }
}

@Component
class SponsorMutations(val dao: SponsorDao) : Mutation {
    @PreAuthorize("hasAnyRole('ADMIN')")
    fun sponsorCreate(sponsor: Sponsor): Sponsor? {
        return dao.save(sponsor)
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    fun sponsorUpdate(event: Sponsor): Sponsor {
        return dao.save(event)
    }
}

@Component
class SponsorRequirementsQuery(val dao: SponsorRequirementsDao) : Query {

    fun sponsorRequirementsFindBudgetGt(volume: Long): List<SponsorRequirements> {
        return dao.findByBudgetGt(volume)
    }

    fun sponsorRequirementsFindBudgetLt(volume: Long): List<SponsorRequirements> {
        return dao.findByBudgetLt(volume)
    }

    fun sponsorRequirementsFindById(id: Long): SponsorRequirements? {
        return dao.searchById(id)
    }
    fun sponsorRequirementsFindPgGt(volume: Int): List<SponsorRequirements> {
        return dao.findByPgGt(volume)
    }
}

@Component
class SponsorRequirementsQueryResolver(val dao: SponsorRequirementsDao) : GraphQLQueryResolver {

    fun sponsorRequirementsFindBudgetGt(volume: Long): List<SponsorRequirements> {
        return dao.findByBudgetGt(volume)
    }

    fun sponsorRequirementsFindBudgetLt(volume: Long): List<SponsorRequirements> {
        return dao.findByBudgetLt(volume)
    }

    fun sponsorRequirementsFindById(id: Long): SponsorRequirements? {
        return dao.searchById(id)
    }
    fun sponsorRequirementsFindPgGt(volume: Int): List<SponsorRequirements> {
        return dao.findByPgGt(volume)
    }
}

@Component
class SponsorRequirementsMutations(val dao: SponsorRequirementsDao) : Mutation {
    @PreAuthorize("hasAnyRole('EVENT_EDITOR')")
    fun sponsorRequirementsCreate(sponsorRequirements: SponsorRequirements): SponsorRequirements? {
        return dao.save(sponsorRequirements)
    }

    @PreAuthorize("hasAnyRole('EVENT_EDITOR')")
    fun sponsorRequirementsUpdate(sponsorRequirements: SponsorRequirements): SponsorRequirements {
        return dao.save(sponsorRequirements)
    }
}
