package com.gitlab.redeyes.theevent.dao.auth

import com.gitlab.redeyes.theevent.dao.BaseDao
import com.gitlab.redeyes.theevent.model.Role
import com.gitlab.redeyes.theevent.model.User
import com.gitlab.redeyes.theevent.repo.RoleRepository
import com.gitlab.redeyes.theevent.repo.UserRepository
import org.springframework.stereotype.Service

@Service
class AuthDao(repo: UserRepository) : BaseDao<User, UserRepository>(repo) {
    fun findAuthUserByUserName(name: String): User? {
        return repo.findByEmail(name)
    }
}

@Service
class RoleDao(repo: RoleRepository) : BaseDao<Role, RoleRepository>(repo) {
    fun findByName(name: String): Role? {
        return repo.findByName(name)
    }
}

