package com.gitlab.redeyes.theevent.dao

import com.gitlab.redeyes.theevent.model.Identified
import com.gitlab.redeyes.theevent.repo.BaseRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import java.util.*

abstract class BaseDao<T : Identified, R : BaseRepository<T>>(protected val repo: R) {
    fun save(entity: T): T {
        return repo.save(entity)
    }

    fun findById(id: Long): Optional<T> {
        return repo.findById(id)
    }

    fun searchById(id: Long): T? {
        return repo.searchById(id)
    }

    fun count(): Long {
        return repo.count()
    }

    fun findAll(of: PageRequest): Page<T> {
        return repo.findAll(of)
    }

    fun findAllByIds(ids: List<Long>): List<T> {
        return repo.findAllById(ids)
    }
}
