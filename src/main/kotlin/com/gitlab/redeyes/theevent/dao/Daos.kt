package com.gitlab.redeyes.theevent.dao

import com.gitlab.redeyes.theevent.model.Media
import com.gitlab.redeyes.theevent.model.MediaRequirements
import com.gitlab.redeyes.theevent.model.Organization
import com.gitlab.redeyes.theevent.model.Place
import com.gitlab.redeyes.theevent.model.PlaceRequirements
import com.gitlab.redeyes.theevent.model.Sponsor
import com.gitlab.redeyes.theevent.model.SponsorRequirements
import com.gitlab.redeyes.theevent.model.SportType
import com.gitlab.redeyes.theevent.model.Team
import com.gitlab.redeyes.theevent.repo.MediaRepository
import com.gitlab.redeyes.theevent.repo.MediaRequirementsRepository
import com.gitlab.redeyes.theevent.repo.OrganizationRepository
import com.gitlab.redeyes.theevent.repo.PlaceRepository
import com.gitlab.redeyes.theevent.repo.PlaceRequirementsRepository
import com.gitlab.redeyes.theevent.repo.SponsorRepository
import com.gitlab.redeyes.theevent.repo.SponsorRequirementsRepository
import com.gitlab.redeyes.theevent.repo.SportTypeRepository
import com.gitlab.redeyes.theevent.repo.TeamRepository
import org.springframework.stereotype.Service


@Service
class TeamDao(repo: TeamRepository) : BaseDao<Team, TeamRepository>(repo)

@Service
class SponsorDao(repo: SponsorRepository) : BaseDao<Sponsor, SponsorRepository>(repo) {
    fun findByName(name: String): List<Sponsor> {
        return repo.findByNameContaining(name)
    }

    fun findByBudgetGt(volume: Long): List<Sponsor> {
        return repo.findByBudgetGreaterThanEqual(volume)
    }

    fun findByBudgetLt(volume: Long): List<Sponsor> {
        return repo.findByBudgetLessThanEqual(volume)
    }
}

@Service
class SponsorRequirementsDao(repo: SponsorRequirementsRepository) : BaseDao<SponsorRequirements, SponsorRequirementsRepository>(repo) {

    fun findByBudgetGt(volume: Long): List<SponsorRequirements> {
        return repo.findByBudgetGreaterThanEqual(volume)
    }

    fun findByBudgetLt(volume: Long): List<SponsorRequirements> {
        return repo.findByBudgetLessThanEqual(volume)
    }

    fun findByPgGt(pg: Int): List<SponsorRequirements> {
        return repo.findByPgGreaterThanEqual(pg)
    }
}

@Service
class PlaceDao(repo: PlaceRepository) : BaseDao<Place, PlaceRepository>(repo)

@Service
class PlaceRequirementsDao(repo: PlaceRequirementsRepository) : BaseDao<PlaceRequirements, PlaceRequirementsRepository>(repo)

@Service
class MediaDao(repo: MediaRepository) : BaseDao<Media, MediaRepository>(repo) {
    fun findByName(name: String): List<Media> {
        return repo.findByNameContaining(name)
    }
}

@Service
class MediaRequirementsDao(repo: MediaRequirementsRepository) : BaseDao<MediaRequirements, MediaRequirementsRepository>(repo)

@Service
class SportTypeDao(repo: SportTypeRepository) : BaseDao<SportType, SportTypeRepository>(repo) {
    fun findAll(): List<SportType> {
        return repo.findAll()
    }

    fun findByName(name: String): SportType? {
        return repo.findOneByName(name)
    }
}


@Service
class OrganizationDao(repo: OrganizationRepository) : BaseDao<Organization, OrganizationRepository>(repo)
