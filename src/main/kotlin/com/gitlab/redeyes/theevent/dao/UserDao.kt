package com.gitlab.redeyes.theevent.dao

import com.gitlab.redeyes.theevent.model.Role
import com.gitlab.redeyes.theevent.model.User
import com.gitlab.redeyes.theevent.repo.RoleRepository
import com.gitlab.redeyes.theevent.repo.UserRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service

@Service
class UserDao(repo: UserRepository) : BaseDao<User, UserRepository>(repo) {
    fun findByName(name: String): User? {
        return repo.findByEmail(name)
    }
}
