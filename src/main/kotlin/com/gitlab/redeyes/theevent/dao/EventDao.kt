package com.gitlab.redeyes.theevent.dao

import com.gitlab.redeyes.theevent.model.*
import com.gitlab.redeyes.theevent.repo.*
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class EventDao(repo: EventRepository) : BaseDao<Event, EventRepository>(repo) {
    fun findByName(name: String): List<Event> {
        return repo.findEventsByNameContaining(name)
    }
}

@Service
class EventBudgetDao(repo: EventBudgetRepository) : BaseDao<EventBudget, EventBudgetRepository>(repo)

@Service
class TaskDao(repo: TaskRepository) : BaseDao<Task, TaskRepository>(repo) {
    fun findAllByAssigneeAndEvent(page: Pageable, user: User, event: Event): Page<Task> {
        return repo.findAllByAssigneeAndEvent(page, user, event)
    }

    fun countByAssigneeAndEvent(user: User, event: Event): Long {
        return repo.countByAssigneeAndEvent(user, event)
    }
}

@Service
class ResourceDao(repo: ResourceRepository) : BaseDao<Resource, ResourceRepository>(repo) {
    fun findAllByEventId(eventId: Long, page: Pageable): Page<Resource> {
        return repo.findAllByEvent_Id(eventId, page)
    }

    fun countByEventId(eventId: Long): Long {
        return repo.countByEvent_Id(eventId)
    }
}
@Service
class ResourceTypeDao(repo: ResourceTypeRepository) : BaseDao<ResourceType, ResourceTypeRepository>(repo)
