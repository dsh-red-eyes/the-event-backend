package com.gitlab.redeyes.theevent.dao

import com.gitlab.redeyes.theevent.model.CheckListItem
import com.gitlab.redeyes.theevent.model.CheckListPlan
import com.gitlab.redeyes.theevent.repo.CheckListItemRepository
import com.gitlab.redeyes.theevent.repo.CheckListPlanRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class CheckListPlanDao(repo: CheckListPlanRepository) : BaseDao<CheckListPlan, CheckListPlanRepository>(repo) {
    fun countByEventId(eventId: Long) : Long {
        return repo.countByEvent_Id(eventId)
    }

    fun findAllByEventId(eventId: Long, page: Pageable): Page<CheckListPlan> {
        return repo.findAllByEvent_Id(eventId, page)
    }
}

@Service
class CheckListItemDao(repo: CheckListItemRepository) : BaseDao<CheckListItem, CheckListItemRepository>(repo) {
    fun findAllByParentId(id: Long): List<CheckListItem> {
        return repo.findAllByParent_Id(id)
    }
}