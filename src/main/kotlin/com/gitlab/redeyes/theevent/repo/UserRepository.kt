package com.gitlab.redeyes.theevent.repo

import com.gitlab.redeyes.theevent.model.Role
import com.gitlab.redeyes.theevent.model.User
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : BaseRepository<User> {
    fun findByEmail(email: String): User?
}

@Repository
interface RoleRepository : BaseRepository<Role> {
    fun findByName(name: String): Role?
}
