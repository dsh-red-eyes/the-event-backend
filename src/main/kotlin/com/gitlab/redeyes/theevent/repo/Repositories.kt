package com.gitlab.redeyes.theevent.repo

import com.gitlab.redeyes.theevent.model.*
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Repository

@Repository
interface OrganizationRepository : BaseRepository<Organization>

@Repository
interface EventRepository : BaseRepository<Event> {
    fun findEventsByNameContaining(namePart: String): List<Event>
}

@Repository
interface EventBudgetRepository : BaseRepository<EventBudget>

@Repository
interface TeamRepository : BaseRepository<Team>

@Repository
interface SponsorRepository : BaseRepository<Sponsor> {
    fun findByNameContaining(name: String): List<Sponsor>
    fun findByBudgetGreaterThanEqual(volume: Long): List<Sponsor>
    fun findByBudgetLessThanEqual(volume: Long): List<Sponsor>
}

@Repository
interface SponsorRequirementsRepository : BaseRepository<SponsorRequirements> {
    fun findByBudgetGreaterThanEqual(volume: Long): List<SponsorRequirements>
    fun findByBudgetLessThanEqual(volume: Long): List<SponsorRequirements>
    fun findByPgGreaterThanEqual(pg: Int): List<SponsorRequirements>
}

@Repository
interface PlaceRepository : BaseRepository<Place>

@Repository
interface PlaceRequirementsRepository : BaseRepository<PlaceRequirements>

@Repository
interface MediaRepository : BaseRepository<Media> {
    fun findByNameContaining(name: String): List<Media>
}

@Repository
interface MediaRequirementsRepository : BaseRepository<MediaRequirements>


@Repository
interface SportTypeRepository : BaseRepository<SportType> {
    fun findOneByName(name: String): SportType?
}

@Repository
interface TaskRepository : BaseRepository<Task> {
    fun findAllByAssigneeAndEvent(page: Pageable, user: User, event: Event): Page<Task>

    fun countByAssigneeAndEvent(user: User, event: Event): Long
}

@Repository
interface ResourceRepository : BaseRepository<Resource> {
    fun countByEvent_Id(eventId: Long): Long
    fun findAllByEvent_Id(eventId: Long, page: Pageable): Page<Resource>
}

@Repository
interface ResourceTypeRepository : BaseRepository<ResourceType>
