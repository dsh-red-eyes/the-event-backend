package com.gitlab.redeyes.theevent.repo

import com.gitlab.redeyes.theevent.model.Identified
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.NoRepositoryBean

@NoRepositoryBean
interface BaseRepository<T : Identified> : JpaRepository<T, Long> {
    fun searchById(id: Long): T?
}
