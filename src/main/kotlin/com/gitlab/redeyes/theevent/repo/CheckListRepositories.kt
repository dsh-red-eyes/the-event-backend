package com.gitlab.redeyes.theevent.repo

import com.gitlab.redeyes.theevent.model.CheckListItem
import com.gitlab.redeyes.theevent.model.CheckListPlan
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Repository

@Repository
interface CheckListPlanRepository : BaseRepository<CheckListPlan> {
    fun countByEvent_Id(eventId: Long): Long
    fun findAllByEvent_Id(eventId: Long, page: Pageable): Page<CheckListPlan>
}

@Repository
interface CheckListItemRepository : BaseRepository<CheckListItem> {
    fun findAllByParent_Id(id: Long): List<CheckListItem>
}

