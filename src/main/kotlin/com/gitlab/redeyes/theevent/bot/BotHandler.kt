package com.gitlab.redeyes.theevent.bot

import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Update

interface MessageHandler {
    fun handle(update: Update, message: String, bot: RedEyesBot)
}

@Component
class RegUser(val repository: TelegramUserRepository) : MessageHandler {
    override fun handle(update: Update, message: String, bot: RedEyesBot) {

        if (!message.startsWith("/reg")) {
            return
        }
        val email = message.substring(5).trim()
        repository.save(TelegramUser(update.message.chatId, email))
    }
}
