package com.gitlab.redeyes.theevent.bot

import com.gitlab.redeyes.theevent.model.BotNotifier
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.stereotype.Service
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.generics.LongPollingBot

@Service
class RedEyesBot(
        @Value("\${bot.token}") private val token: String,
        @Autowired val actions: List<MessageHandler>
) : TelegramLongPollingBot() {
    override fun getBotUsername(): String {
        return "RedEyesHackathon2020bot"
    }

    override fun getBotToken(): String {
        return token
    }

    override fun onUpdateReceived(update: Update) {
        update.message.text?.let {
            actions.forEach { action -> action.handle(update, it, this) }
        }
    }

}

@Service
class TelegramBotNotifier(val bot: TelegramLongPollingBot, val userRepository: TelegramUserRepository) : BotNotifier {
    override fun notify(email: String, message: String) {
        userRepository.findByEmail(email)?.let { bot.execute(SendMessage(it.id, message)) }
    }
}
