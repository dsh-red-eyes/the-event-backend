package com.gitlab.redeyes.theevent.bot

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "telegram_users")
class TelegramUser(
        @Id
        var id: Long?,
        var email: String?) {
    constructor() : this(null, null)
}

@Repository
interface TelegramUserRepository : JpaRepository<TelegramUser, Long> {
    fun findByEmail(email: String): TelegramUser?
}
