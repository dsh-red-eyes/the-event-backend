package com.gitlab.redeyes.theevent.service

import com.gitlab.redeyes.theevent.dao.auth.AuthDao
import com.gitlab.redeyes.theevent.model.auth.AuthUserDetails
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.ReactiveUserDetailsService
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service("authUserDetails")
class AuthUserDetailsService(@Autowired val authDao: AuthDao) : ReactiveUserDetailsService {
    override fun findByUsername(userName: String): Mono<UserDetails> {
        return Mono.justOrEmpty(authDao.findAuthUserByUserName(userName))
                .map { AuthUserDetails(it) }
    }
}
