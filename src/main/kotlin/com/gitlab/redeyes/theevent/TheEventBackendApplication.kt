package com.gitlab.redeyes.theevent

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.telegram.telegrambots.ApiContextInitializer

@SpringBootApplication
@EnableJpaRepositories
class TheEventBackendApplication

fun main(args: Array<String>) {
    ApiContextInitializer.init();
    runApplication<TheEventBackendApplication>(*args)
}
