package com.gitlab.redeyes.theevent.model.auth

import com.gitlab.redeyes.theevent.service.AuthUserDetailsService
import io.jsonwebtoken.Claims
import io.jsonwebtoken.ExpiredJwtException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.ReactiveSecurityContextHolder
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextImpl
import org.springframework.security.web.server.context.ServerSecurityContextRepository
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import java.util.*


@Component
class AuthenticationManager(@Autowired
                            private val jwtUtil: JwtUtils) : ReactiveAuthenticationManager {

    override fun authenticate(authentication: Authentication): Mono<Authentication> {
        val authToken = authentication.credentials.toString()
        try {
            if (jwtUtil.isTokenExpired(authToken)!!) {
                return Mono.empty()
            }
            val claims: Claims = jwtUtil.getAllClaimsFromToken(authToken)
            val authorities: MutableList<GrantedAuthority> = ArrayList()
            return Mono.just(UsernamePasswordAuthenticationToken(claims.subject, null, authorities))
        } catch (e: Exception) {
            return Mono.empty()
        }
    }
}


@Component
class SecurityContextRepository(val jwtUtil: JwtUtils, val authUserDetailsService: AuthUserDetailsService) : ServerSecurityContextRepository {
    @Autowired
    private val authenticationManager: AuthenticationManager? = null

    override fun save(swe: ServerWebExchange?, sc: SecurityContext?): Mono<Void> {
        throw UnsupportedOperationException("Not supported yet.")
    }

    override fun load(swe: ServerWebExchange): Mono<SecurityContext> {
        val request: ServerHttpRequest = swe.request
        val authHeader: String? = request.headers.getFirst(HttpHeaders.AUTHORIZATION)
        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            val authToken = authHeader.substring(7)
            return try {
                val claims: Claims = jwtUtil.getAllClaimsFromToken(authToken)
                val userDetailMono = authUserDetailsService.findByUsername(claims.subject)
                userDetailMono.flatMap {
                    val auth: Authentication = UsernamePasswordAuthenticationToken(claims.subject, authToken, it.authorities)
                    ReactiveSecurityContextHolder.withAuthentication(auth)
                    authenticationManager!!.authenticate(auth).map<SecurityContext> { authentication: Authentication? -> SecurityContextImpl(authentication) }
                }
            } catch (e: ExpiredJwtException) {
                Mono.empty<SecurityContext>()
            }
        } else {
            return Mono.empty<SecurityContext>()
        }
    }
}
