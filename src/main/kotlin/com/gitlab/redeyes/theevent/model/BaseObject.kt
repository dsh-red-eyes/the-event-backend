package com.gitlab.redeyes.theevent.model

import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.MappedSuperclass

@MappedSuperclass
abstract class Identified(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        var id: Long?
) {
    constructor() : this(null)
}

@MappedSuperclass
abstract class Named(var name: String) : Identified(null)
