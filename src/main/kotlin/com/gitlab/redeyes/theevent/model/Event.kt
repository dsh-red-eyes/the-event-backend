package com.gitlab.redeyes.theevent.model

import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.Index
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "events", indexes = [Index(name = "event_idx", unique = true, columnList = "name")])
class Event(
        @Column(nullable = false)
        var name: String,
        @ManyToOne
        val owner: User,
        var startDate: LocalDateTime,
        var endDate: LocalDateTime,
        var plannedMembersCount: Int,
        var budget: Long,
        @Enumerated
        var state: EventState,
        @ManyToOne
        var sportType: SportType,
//        @ManyToOne(fetch = FetchType.LAZY)
//        var place: Place?,
//        @ManyToMany(fetch = FetchType.LAZY)
//        var sponsor: List<Sponsor>,
//        @OneToMany(fetch = FetchType.LAZY)
//        var media: List<Media>,
//        @OneToMany(fetch = FetchType.LAZY)
//        var keyPersons: List<User>,
//        @OneToMany(fetch = FetchType.LAZY)
//        val teams: List<Team>,
//        @OneToMany(fetch = FetchType.LAZY)
//        val players: List<User>,
        @OneToMany(fetch = FetchType.LAZY)
        var organizingCommittee: List<User>?,
        @OneToMany(fetch = FetchType.LAZY)
        var organizators: List<User>?,
        @OneToMany(fetch = FetchType.LAZY)
        var volunteers: List<Volunteer>?,
        @ManyToOne
        val responsible: User
) : Identified()

enum class EventState {
    CREATED, REGISTRATION, REGISTRATION_COMPLETED, STARTED, COMPLETED
}

@Entity
class EventBudget(
        var volume: Long,
        @Enumerated
        val type: EventBudgetType,
        var percentage: Int,
        @OneToOne
        var event: Event
) : Identified()


enum class EventBudgetType {
    EXTERNAL, SPONSORED, MEMBER_FEE
}
