package com.gitlab.redeyes.theevent.model

import javax.persistence.*

@Entity
@Table(name = "resources")
class Resource(
        var name: String,
        @ManyToOne
        var event: Event,
        var requirements: String?,
        var resolution: String?,
        @Enumerated
        var state: ApprovalProcessState,
        var budget: Double?,
        @ManyToOne(fetch = FetchType.LAZY)
        var assignee: User?,
        @ManyToOne
        val type: ResourceType
): Identified()

@Entity
@Table(name = "resource_types")
class ResourceType(
        val name: String,
        val description: String?
): Identified()