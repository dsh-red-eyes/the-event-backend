package com.gitlab.redeyes.theevent.model.auth

import com.gitlab.redeyes.theevent.model.User
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.security.Key

import java.util.*
import javax.annotation.PostConstruct

@Component
class JwtUtils {
    @Value("\${auth.jjwt.secret}")
    private val secret: String? = null

    @Value("\${auth.jjwt.expiration}")
    private val expirationTime: String? = null

    private var key: Key? = null

    @PostConstruct
    fun init() {
        key = Keys.hmacShaKeyFor(secret!!.toByteArray())
    }

    fun getAllClaimsFromToken(token: String?): Claims {
        return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).body
    }

    fun getUsernameFromToken(token: String?): String? {
        return getAllClaimsFromToken(token).subject
    }

    fun getExpirationDateFromToken(token: String?): Date {
        return getAllClaimsFromToken(token).expiration
    }

    fun isTokenExpired(token: String): Boolean? {
        val expiration: Date = getExpirationDateFromToken(token)
        return expiration.before(Date())
    }

    fun generateToken(user: User): String? {
        return doGenerateToken(user.email)
    }

    private fun doGenerateToken(username: String): String? {
        val expirationTimeLong = expirationTime!!.toLong() //in second
        val createdDate = Date()
        val expirationDate = Date(createdDate.time + expirationTimeLong * 1000)
        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(key)
                .compact()
    }

    fun validateToken(token: String): Boolean? {
        return !isTokenExpired(token)!!
    }
}
