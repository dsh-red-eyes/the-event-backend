package com.gitlab.redeyes.theevent.model

import javax.persistence.*

@Entity
@Table(name = "tasks")
class Task(
    @ManyToOne
    var event: Event,
    var name: String,
    var description: String,
    @ManyToOne(fetch = FetchType.LAZY)
    val creator: User?,
    @ManyToOne(fetch = FetchType.LAZY)
    var assignee: User?,
    @ManyToOne
    var resource: Resource?,
    @Enumerated
    var state: ApprovalProcessState
): Identified()