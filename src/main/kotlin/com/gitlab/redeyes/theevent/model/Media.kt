package com.gitlab.redeyes.theevent.model

import javax.persistence.Entity

@Entity
class Media(
        val name: String,
        val pg: Int
) : Identified()

@Entity
class MediaRequirements(val pg: Int) : Identified()
