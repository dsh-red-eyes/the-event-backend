package com.gitlab.redeyes.theevent.model

import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.UniqueConstraint


@Entity
@Table(uniqueConstraints = [UniqueConstraint(name = "team_name_constr", columnNames = ["name"])])
class Team(
        var name: String,
        @OneToMany
        val participants: List<User>,
        @ManyToOne
        val captain: User
) : Identified()
