package com.gitlab.redeyes.theevent.model.auth

import org.springframework.beans.factory.annotation.Value
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import java.security.NoSuchAlgorithmException
import java.security.spec.InvalidKeySpecException
import java.util.*
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec


class AuthRequest(val username: String, val password: String)
class AuthResponse(val token: String?)


@Component
class PBKDF2Encoder : PasswordEncoder {
    @Value("\${auth.password.encoder.secret}")
    private val secret: String? = null
    @Value("\${auth.password.encoder.iteration}")
    private val iteration: Int? = null
    @Value("\${auth.password.encoder.keylength}")
    private val keylength: Int? = null

    /**
     * More info (https://www.owasp.org/index.php/Hashing_Java)
     * @param cs password
     * @return encoded password
     */
    override fun encode(cs: CharSequence): String {
        return try {
            val result = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512")
                    .generateSecret(PBEKeySpec(cs.toString().toCharArray(), secret!!.toByteArray(), iteration!!, keylength!!))
                    .encoded
            Base64.getEncoder().encodeToString(result)
        } catch (ex: NoSuchAlgorithmException) {
            throw RuntimeException(ex)
        } catch (ex: InvalidKeySpecException) {
            throw RuntimeException(ex)
        }
    }

    override fun matches(raw: CharSequence, encoded: String): Boolean {
        return encode(raw) == encoded
    }
}
