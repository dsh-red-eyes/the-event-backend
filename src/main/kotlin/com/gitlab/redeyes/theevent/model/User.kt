package com.gitlab.redeyes.theevent.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.ManyToMany
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(
        name = "users",
        uniqueConstraints = [UniqueConstraint(name = "email_contr", columnNames = ["email"])]
)
class User(var fname: String,
           var lname: String,
           @Column(nullable = false)
           val email: String,
           @OneToOne(optional = true)
           val organization: Organization?,
           var password: String,
           @ManyToMany(fetch = FetchType.EAGER)
           var roles: List<Role>
) : Identified(null)
