package com.gitlab.redeyes.theevent.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "organizations")
class Organization(name: String) : Named(name)
