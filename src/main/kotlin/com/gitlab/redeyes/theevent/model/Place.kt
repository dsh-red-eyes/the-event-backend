package com.gitlab.redeyes.theevent.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "places")
class Place(
        @Column(nullable = false)
        var name: String,
        @Column(nullable = false)
        var capacity: Int,
        @Column(nullable = false)
        var address: String,
        @OneToMany
        val sportTypes: List<SportType>
) : Identified()

@Entity
@Table(name = "place_requirements")
class PlaceRequirements(
        @OneToMany
        val sportTypes: List<SportType>,
        var capacity: Int?,
        var city: String?,
        @ManyToOne
        val event: Event
) : Identified()
