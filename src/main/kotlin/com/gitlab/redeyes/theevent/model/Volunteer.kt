package com.gitlab.redeyes.theevent.model

import javax.persistence.*

@Entity
@Table(
        name = "volunteers"
)
class Volunteer(
        var name: String,
        @ManyToOne(optional = true)
        val organization: Organization,
        var contact: String
): Identified()