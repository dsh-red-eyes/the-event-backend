package com.gitlab.redeyes.theevent.model

import javax.persistence.Entity
import javax.persistence.ManyToOne

@Entity
class Sponsor(
        var budget: Long,
        var name: String,
        var pgs: Int
) : Identified()

@Entity
class SponsorRequirements(
        var budget: Long,
        var pg: Int,
        @ManyToOne
        val event: Event
) : Identified()
