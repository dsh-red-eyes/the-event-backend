package com.gitlab.redeyes.theevent.model

import javax.persistence.*

@Entity
@Table(name = "check_list_plan")
class CheckListPlan(
        @ManyToOne
        var event: Event,
        @Column(nullable = false)
        var name: String,
        @OneToOne
        var author: User,
        @OneToOne
        @JoinColumn(name = "root_item_id")
        var root: CheckListItem
) : Identified()

@Entity
@Table(name = "check_list_items")
class CheckListItem(
        @ManyToOne
        @JoinColumn(name = "parent_id")
        var parent: CheckListItem?,
        @OneToMany(fetch = FetchType.LAZY, mappedBy = "parent", cascade = [CascadeType.ALL], orphanRemoval = true)
        var subItems: MutableList<CheckListItem>,
        @Column(nullable = false)
        var name: String,
        @ManyToOne
        var author: User,
        @ManyToOne
        var assignee: User?,
        var spent: Double,
        @OneToMany(fetch = FetchType.LAZY)
        var resources: List<Resource>?,
        var completed: Boolean
) : Identified()

enum class ApprovalProcessState {
    OPEN, COMPLETED, UNAPPROVED, APPROVED, REJECTED
}
