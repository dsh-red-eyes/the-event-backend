package com.gitlab.redeyes.theevent.model

import javax.persistence.Entity


@Entity
class SportType(val name: String) : Identified()
