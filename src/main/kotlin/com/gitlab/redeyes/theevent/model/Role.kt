package com.gitlab.redeyes.theevent.model

import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(
        name = "roles",
        uniqueConstraints = [UniqueConstraint(name = "auth_role_contr", columnNames = ["name"])]
)
class Role(name: String) : Named(name) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (other is Role) {
            return other.name === name
        }
        return false
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}
