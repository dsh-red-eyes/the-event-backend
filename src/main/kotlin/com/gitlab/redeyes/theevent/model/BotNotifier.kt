package com.gitlab.redeyes.theevent.model

interface BotNotifier {
    fun notify(email: String, message: String)
}
