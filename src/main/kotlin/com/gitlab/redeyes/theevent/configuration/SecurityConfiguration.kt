package com.gitlab.redeyes.theevent.configuration

import com.gitlab.redeyes.theevent.model.auth.AuthenticationManager
import com.gitlab.redeyes.theevent.model.auth.SecurityContextRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.web.server.SecurityWebFilterChain


@Configuration
@EnableWebFluxSecurity
class SecurityConfiguration(
        @Autowired
        val authenticationManager: AuthenticationManager,
        @Autowired
        val securityContextRepository: SecurityContextRepository
) {

    @Bean
    fun configure(http: ServerHttpSecurity): SecurityWebFilterChain? {
        return http
                .csrf().disable()
                .formLogin().disable()
                .httpBasic().disable()
                .authenticationManager(authenticationManager)
                .securityContextRepository(securityContextRepository)
                .authorizeExchange()
                .pathMatchers(HttpMethod.OPTIONS).permitAll()
                .pathMatchers("/auth").permitAll()
                .pathMatchers("/playground").permitAll()
                .anyExchange().authenticated()
                .and().build();
    }

}
