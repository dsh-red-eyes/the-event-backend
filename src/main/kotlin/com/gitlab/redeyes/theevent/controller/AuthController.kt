package com.gitlab.redeyes.theevent.controller

import com.gitlab.redeyes.theevent.model.auth.AuthRequest
import com.gitlab.redeyes.theevent.model.auth.AuthResponse
import com.gitlab.redeyes.theevent.model.auth.AuthUserDetails
import com.gitlab.redeyes.theevent.model.auth.JwtUtils
import com.gitlab.redeyes.theevent.model.auth.PBKDF2Encoder
import com.gitlab.redeyes.theevent.service.AuthUserDetailsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/")
class AuthenticationREST(
        @Autowired
        val jwtUtil: JwtUtils,
        @Autowired
        val passwordEncoder: PBKDF2Encoder,
        @Autowired
        val userService: AuthUserDetailsService
) {

    @PostMapping("/auth", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun login(@RequestBody rq: AuthRequest): Mono<ResponseEntity<AuthResponse>> {
        return userService.findByUsername(rq.username)
                .map {
                    if (passwordEncoder.matches(rq.password, it.password)) {
                        ResponseEntity.ok(AuthResponse(jwtUtil.generateToken((it as AuthUserDetails).user)))
                    } else {
                        ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()
                    }
                }
    }

    @PostMapping("/auth", consumes = [MediaType.APPLICATION_FORM_URLENCODED_VALUE])
    fun authentication(exchange: ServerWebExchange): Mono<ResponseEntity<AuthResponse>> {
        return exchange.formData.map {
            AuthRequest(it.getFirst("username")!!, it.getFirst("password")!!)
        }.flatMap { rq ->
            userService.findByUsername(rq.username)
                    .map {
                        if (passwordEncoder.encode(rq.password) == it.password) {
                            ResponseEntity.ok(AuthResponse(jwtUtil.generateToken((it as AuthUserDetails).user)))
                        } else {
                            ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()
                        }
                    }
        }
    }

}
