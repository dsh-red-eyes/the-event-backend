package com.gitlab.redeyes.theevent.controller

import org.springframework.context.annotation.Bean
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.server.bodyValueAndAwait
import org.springframework.web.reactive.function.server.coRouter
import org.springframework.web.reactive.function.server.html
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/")
class WelcomeController {
    @GetMapping
    fun welcome(): Mono<String> = Mono.just("welcome")

    @Bean
    fun reg() = coRouter {
        GET("/crouter") {
            ok().html().bodyValueAndAwait("<h1>ok</h1>")
        }
    }
}


@RestController
@RequestMapping("/secure")
class SecureWelcomeController {
    @GetMapping
    fun welcome(): String = "Secured"
}
