FROM openjdk:8-jre-slim-buster
WORKDIR "/home/"
EXPOSE 8080
ADD ./build ./
ADD ./target/the-event-backend-0.0.1-SNAPSHOT.jar ./
RUN chmod +x run.sh
CMD ./run.sh