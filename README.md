# the-event-backend

## Build
```bash
$ mvn clean package
```
## Run application
```bash
$ docker-compose up --build
```