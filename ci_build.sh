#!/bin/sh
if [ $CI_COMMIT_REF_NAME = "master" ]; then
    docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    export IMAGE_NAME=$CI_REGISTRY/dsh-red-eyes/the-event-backend/the-event-backend
    export VERSION=$(cat pom.xml | grep -A 1 "<artifactId>the-event-backend</artifactId>" | grep version | cut -d '>' -f 2 | cut -d '<' -f 1)
    docker build -t $IMAGE_NAME:$VERSION .
    docker push $IMAGE_NAME:$VERSION
    docker tag $IMAGE_NAME:$VERSION $IMAGE_NAME:latest
    docker push $IMAGE_NAME:latest
fi
